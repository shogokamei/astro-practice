# astro-practice

## 初回起動
ルートに`app`ディレクトリがない場合は作成  

以下コメントを実行
```
[Win]docker-compose up -d --build
```

```
[app]docker exec -it astro-app sh
[app]npm create astro@latest
[app]cd project_name
[app]npm install
```

package.jsonのScriptを修正
```
"dev": "astro dev --host 0.0.0.0 --port 3000",
```

サーバを起動
```
[app]npm run dev
```

## 2回目以降の起動
以下コメントを実行

```
[Win]docker-compose up
[app]docker exec -it astro-app sh
[app]cd project_name
[app]npm run dev
```

## astroとMicroCMSを紐づけ
出典 https://blog.microcms.io/astro-microcms-introduction/

### 1.環境変数を設定
.env_exampleをコピーし、.envファイルにリネーム  
中身を適宜変更

### 2.microCMS JavaScript SDKのインストール
```
[app]npm install microcms-js-sdk
```

### 3.API呼び出し用のライブラリを作成
`src/library/microcms.ts` にAPI呼び出し用APIを実装

### 4.viewの作成
任意でvscodeにastroの拡張機能をいれておく  
`src/pages/` 内に任意のastroファイルを作成しViewを実装
※ホットリロードが効かない場合  
`astro.config.mjs` の`defineConfig`に以下を追記
```
vite: {
    server: {
        watch: {
            usePolling: true,
            interval: 1000
        }
    }
}
```

## 静的ビルド
以下コメントを実行するとプロジェクト内に`dist`ディレクトリが生成される
```
[app]npm run build
```

## MicroCMSのwebhookを使用した自動デプロイ
今回はMicroCMSの更新をフックにGitHubを経由しエックスサーバにデプロイする方法を想定  

### 1.MicroCMSからWebhookを設定
`MicroCMSの管理画面 > API設定 > Webhook`から`GitHub Actions`を選択し設定  
※GitHubのアクセストークンの用意が必要  
`GitHub > Setting > Developer Settings`からトークンを作成

### 2.gitの設定ファイルを作成
`.github/workflows`内に任意の名前の`yml`ファイルを作成
```
name: Deploy to GitHub Pages
on:
  push:
    branches: [ main ]
  workflow_dispatch:
  repository_dispatch:
    branches: [ main ]
    types: [ MicroCMSで設定したアクション名 ]

permissions:
  contents: read
  pages: write
  id-token: write

jobs:
  build-and-deploy:
    runs-on: ubuntu-latest

    steps:
      - name: Checkout repository
        uses: actions/checkout@v3

      - name: Setup Node.js
        uses: actions/setup-node@v3
        with:
          node-version: '20'

      - name: Retrieve the secrets and put them to .env
        env:
          MICROCMS_SERVICE_DOMAIN: ${{ secrets.MICROCMS_SERVICE_DOMAIN }}
          MICROCMS_API_KEY: ${{ secrets.MICROCMS_API_KEY }}
        run: |
          echo MICROCMS_SERVICE_DOMAIN=$MICROCMS_SERVICE_DOMAIN >> $GITHUB_WORKSPACE/.env
          echo MICROCMS_API_KEY=$MICROCMS_API_KEY >> $GITHUB_WORKSPACE/.env
          ls -la $GITHUB_WORKSPACE/.env
          wc -l $GITHUB_WORKSPACE/.env

      - name: Install dependencies
        run: npm install

      - name: Build the project
        run: npm run build

      - name: Sync files
        uses: SamKirkland/FTP-Deploy-Action@v4.3.4
        with:
          server: ${{ secrets.FTP_SERVER }}
          username: ${{ secrets.FTP_USERNAME }}
          password: ${{ secrets.FTP_PASSWORD }}
          local-dir: ./dist/
          server-dir: ${{ secrets.FTP_SERVER_DIR }}
          protocol: ftp
```

### 3.Githubで環境変数を設定
対象プロジェクトのリポジトリから  
`Settings > Secrets and variables > Actions`から`New repository secret`をクリックし`secrets`に今回は以下設定を登録する
```
MICROCMS_API_KEY
FTP_SERVER
FTP_USERNAME
FTP_PASSWORD
MICROCMS_SERVICE_DOMAIN
FTP_SERVER_DIR（末尾は/で終わる必要あり）
```

### 4.動作チェック
記事を作成、更新、削除し指定したサーバ上にデプロイされるか確認
アクションの結果やログの確認は対象プロジェクトのリポジトリ`Actions`から確認可能